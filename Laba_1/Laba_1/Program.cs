﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

 namespace Laba_1
{
    class Program
    {
        static List<Note> bloknot = new List<Note>();//Блакнот
        static void Main(string[] args)
        {
            Terminal();
        }
        static public void Terminal()
        {
            Note note_main = new Note();//Лист
            string numberNote;

            Console.WriteLine("Выберете опирацию");
            Console.WriteLine("1 - добавить запись");
            Console.WriteLine("2 - редоктировать запись");
            Console.WriteLine("3 - удалить запись");
            Console.WriteLine("4 - просмотр записи");
            Console.WriteLine("5 - просмотр всех созданных записей\n");

            switch (Console.ReadLine())
            {
                case "1":
                    bloknot.Add(note_main.Add(note_main));
                    break;

                case "2":
                    Console.WriteLine("Выберете номер записи");
                    Console.Write("Все доступные записи: ");
                    for (int i = 0; i < bloknot.Count; i++)
                    {
                        Console.Write("№" + i + " ");
                    }
                    Console.WriteLine();
                    numberNote = Console.ReadLine();
                    while (!numberNote.ToCharArray().All(char.IsDigit))
                    {
                        Console.WriteLine("Ошибка попробуйте ещё раз)");
                        numberNote = Console.ReadLine();
                    }
                    bloknot[Convert.ToInt32(numberNote)].Edit();
                    break;

                case "3":
                    Console.WriteLine("Выберете номер записи");
                    Console.Write("Все доступные записи: ");
                    for (int i = 0; i < bloknot.Count; i++)
                    {
                        Console.Write("№" + i + " ");
                    }
                    Console.WriteLine();
                    numberNote = Console.ReadLine();
                    while (!numberNote.ToCharArray().All(char.IsDigit))
                    {
                        Console.WriteLine("Ошибка попробуйте ещё раз)");
                        numberNote = Console.ReadLine();
                    }
                    bloknot[Convert.ToInt32(numberNote)].View();
                    bloknot[Convert.ToInt32(numberNote)].Delete();
                    break;

                case "4":
                    Console.Write("Все доступные записи: ");
                    for (int i = 0; i < bloknot.Count; i++)
                    {
                        Console.Write("№" + i + " ");
                    }
                    Console.WriteLine();
                    Console.WriteLine("Выберете номер записи");
                    numberNote = Console.ReadLine();
                    while (!numberNote.ToCharArray().All(char.IsDigit))
                    {
                        Console.WriteLine("Ошибка попробуйте ещё раз)");
                        numberNote = Console.ReadLine(); 
                    }
                    bloknot[Convert.ToInt32(numberNote)].View();
                    break;

                case "5":
                    Note.ViewAll();
                    break;
                default:
                    Console.WriteLine("Ошибка попробуйте ещё раз)\n");
                    Terminal();
                    break;
            }
            Terminal();
        }
        public class Note
        {
            public string SurName { get; set; }
            public string Name { get; set; } 
            public string Patronymic { get; set; }
            public string Telephone { get; set; }
            public string Country { get; set; }
            public string DateBirth { get; set; }
            public string Organization { get; set; }
            public string Position { get; set; }
            public string Notes { get; set; }
            private bool Start { get; set; } = true;

            public Note Add(Note note)
            {
                Console.WriteLine($"Лист №{bloknot.Count}\n");
                if (Start == true)
                {
                    do//Фамилия
                    {
                        Console.WriteLine("Введите фамилию");
                        SurName = Console.ReadLine();
                    } while (SurName == "");

                    do//Имя
                    {
                        Console.WriteLine("\nВведите имя");
                        Name = Console.ReadLine();
                    } while (Name == "");

                    do//Телефон
                    {
                        Console.WriteLine("\nВведите номер телефон");
                        Telephone = Console.ReadLine();
                        if (!Telephone.ToCharArray().All(char.IsDigit) && (Telephone[0] != '-'))
                        {
                            Telephone = "";
                        }
                    } while (Telephone == "");

                    do//Страна
                    {
                        Console.WriteLine("\nВведите страну");
                        Country = Console.ReadLine();
                    } while (Country == "");

                    Start = false;
                }
                Console.WriteLine("\nВыберете какую запись вы хотите добавить");
                switch (Option())
                {
                    case "1"://Обязательно для ввода
                        do//Фамилия
                        {
                            Console.WriteLine("Введите фамилию");
                            SurName = Console.ReadLine();
                        } while (SurName == "");
                        Add(note);
                        break;

                    case "2"://Обязательно для ввода
                        do//Имя
                        {
                            Console.WriteLine("Введите имя");
                            Name = Console.ReadLine();
                        } while (Name == "");
                        Add(note);
                        break;

                    case "3":
                        Console.WriteLine("Введите отчество");
                        Patronymic = Console.ReadLine();//Отчество
                        Add(note);
                        break; 

                    case "4"://Обязательно для ввода
                        do//Телефон
                        {
                            Console.WriteLine("Введите номер телефон");
                            Telephone = Console.ReadLine();
                            if (!Telephone.ToCharArray().All(char.IsDigit) && (Telephone[0] != '-'))
                            {
                                Telephone = "";
                            }
                        } while (Telephone == "");
                        Add(note);
                        break;

                    case "5"://Обязательно для ввода
                        do//Страна
                        {
                            Console.WriteLine("Введите страну");
                            Country = Console.ReadLine();
                        } while (Country == "");
                        Add(note);
                        break;

                    case "6":
                        Console.WriteLine("Введите дату рождения");
                        DateBirth = Console.ReadLine();
                        Add(note);
                        break;

                    case "7":
                        Console.WriteLine("Введите органицазию");
                        Organization = Console.ReadLine();
                        Add(note);
                        break;

                    case "8":
                        Console.WriteLine("Введите должность");
                        Position = Console.ReadLine();
                        Add(note);
                        break;

                    case "9":
                        Console.WriteLine("Введите всё что угодно");
                        Notes = Console.ReadLine();
                        Add(note);
                        break;

                    case "10":
                        return note;
                }
                return note;
            }

            public void Edit()
            {
                Console.WriteLine("Выберете какую запись вы хотите изменить");
                switch (Option())
                {
                    case "1":
                        Console.WriteLine(SurName);
                        SurName = Console.ReadLine();
                        break;

                    case "2":
                        Console.WriteLine(Name);
                        Name = Console.ReadLine();
                        break;

                    case "3":
                        Console.WriteLine(Patronymic);
                        Patronymic = Console.ReadLine();
                        break;

                    case "4":
                        Console.WriteLine(Telephone);
                        do//Телефон
                        {
                            Telephone = Console.ReadLine();
                            if (!Telephone.ToCharArray().All(char.IsDigit) && (Telephone[0] != '-'))
                            {
                                Telephone = "";
                            }
                        } while (Telephone == "");
                        break;

                    case "5":
                        Console.WriteLine(Country);
                        do//Страна
                        {
                            Telephone = Console.ReadLine();
                            if (Country.ToCharArray().All(char.IsDigit))
                            {
                                Country = "";
                            }
                        } while (Country == "");
                        break;

                    case "6":
                        Console.WriteLine(DateBirth);
                        DateBirth = Console.ReadLine();
                        break;

                    case "7":
                        Console.WriteLine(Organization);
                        Organization = Console.ReadLine();
                        break;

                    case "8":
                        Console.WriteLine(Position);
                        Position = Console.ReadLine();
                        break;

                    case "9":
                        Console.WriteLine(Notes);
                        Notes = Console.ReadLine();
                        break;

                    case "10":
                        Terminal();
                        break;
                }
            }

            public void Delete()
            {
                Console.WriteLine("Выберете какую запись вы хотите удалить");
                switch (Option())
                {
                    case "1":
                        if (SurName == null)
                        {
                            Console.WriteLine("Данное поле не заполнено");
                            Delete();
                        }
                        Console.WriteLine(SurName);
                        Console.WriteLine("Удалить y/n");
                        if (Console.ReadLine() == "y")
                        {
                            SurName = "";
                        }
                        else
                        {
                            Delete();
                        }
                        break;

                    case "2":
                        if (Name == null)
                        {
                            Console.WriteLine("Данное поле не заполнено");
                            Delete();
                        }
                        Console.WriteLine(Name);
                        Console.WriteLine("Удалить y/n");
                        if (Console.ReadLine() == "y")
                        {
                            Name = "";
                        }
                        else
                        {
                            Delete();
                        }
                        break;

                    case "3":
                        if (Patronymic == null)
                        {
                            Console.WriteLine("Данное поле не заполнено");
                            Delete();
                        }
                        Console.WriteLine(Patronymic);
                        Console.WriteLine("Удалить y/n");
                        if (Console.ReadLine() == "y")
                        {
                            Patronymic = "";
                        }
                        else
                        {
                            Delete();
                        }
                        break;

                    case "4":
                        if (Telephone == null)
                        {
                            Console.WriteLine("Данное поле не заполнено");
                            Delete();
                        }
                        Console.WriteLine(Telephone);
                        Console.WriteLine("Удалить y/n");
                        if (Console.ReadLine() == "y")
                        {
                            Telephone = "";
                        }
                        else
                        {
                            Delete();
                        }
                        break;

                    case "5":
                        if (Country == null)
                        {
                            Console.WriteLine("Данное поле не заполнено");
                            Delete();
                        }
                        Console.WriteLine(Country);
                        Console.WriteLine("Удалить y/n");
                        if (Console.ReadLine() == "y")
                        {
                            Country = "";
                        }
                        else
                        {
                            Delete();
                        }
                        break;

                    case "6":
                        if (DateBirth == null)
                        {
                            Console.WriteLine("Данное поле не заполнено");
                            Delete();
                        }
                        Console.WriteLine(DateBirth);
                        Console.WriteLine("Удалить y/n");
                        if (Console.ReadLine() == "y")
                        {
                            DateBirth = "";
                        }
                        else
                        {
                            Delete();
                        }
                        break;

                    case "7":
                        if (Organization == null)
                        {
                            Console.WriteLine("Данное поле не заполнено");
                            Delete();
                        }
                        Console.WriteLine(Organization);
                        Console.WriteLine("Удалить y/n");
                        if (Console.ReadLine() == "y")
                        {
                            Organization = "";
                        }
                        else
                        {
                            Delete();
                        }
                        break;

                    case "8":
                        if (Position == null)
                        {
                            Console.WriteLine("Данное поле не заполнено");
                            Delete();
                        }
                        Console.WriteLine(Position);
                        Console.WriteLine("Удалить y/n");
                        if (Console.ReadLine() == "y")
                        {
                            Position = null;
                        }
                        else
                        {
                            Delete();
                        }
                        break;

                    case "9":
                        if (Notes == null)
                        {
                            Console.WriteLine("Данное поле не заполнено");
                            Delete();
                        }
                        Console.WriteLine(Notes);
                        Console.WriteLine("Удалить y/n");
                        if (Console.ReadLine() == "y")
                        {
                            Notes = "";
                        }
                        else
                        {
                            Delete();
                        }
                        break;

                    case "10":
                        Terminal();
                        break;
                }
            }

            public void View()
            {
                Console.WriteLine($"Фамилия - {SurName}");
                Console.WriteLine($"Имя - {Name}");
                Console.WriteLine($"Отчество - {Patronymic}");
                Console.WriteLine($"Телефон - {Telephone}");
                Console.WriteLine($"Страна - {Country}");
                Console.WriteLine($"Дата рождения - {DateBirth}");
                Console.WriteLine($"Организация - {Organization}");
                Console.WriteLine($"Должность - {Organization}");
                Console.WriteLine($"Прочее - {Notes}");
            }

            public static void ViewAll()
            {
                for (int i = 0; i < bloknot.Count; i++)
                {
                    Console.WriteLine($"Лист №{i}");
                    Console.WriteLine(bloknot[i].SurName);
                    Console.WriteLine(bloknot[i].Name);
                    Console.WriteLine(bloknot[i].Telephone);
                    Console.WriteLine();
                }
            }

            public string Option()
            {
                Console.WriteLine("1 - Фамилия");
                Console.WriteLine("2 - Имя");
                Console.WriteLine("3 - Отчество");
                Console.WriteLine("4 - Телефон");
                Console.WriteLine("5 - Страна");
                Console.WriteLine("6 - Дата рождения");
                Console.WriteLine("7 - Организация");
                Console.WriteLine("8 - Должность");
                Console.WriteLine("9 - Прочее");
                Console.WriteLine("10 - Вернуться назад");
                Console.WriteLine();

                string result = Console.ReadLine();
                try
                {
                    if ((Convert.ToInt32(result) >= 1) && (Convert.ToInt32(result) <= 10))
                    {
                        return result;
                    }
                    else
                    {
                        Console.WriteLine("Ошибка попробуйте ещё раз)");
                        Option();
                        return result;
                    }
                }
                catch
                {
                    Console.WriteLine("Ошибка попробуйте ещё раз)");
                    Option();
                    return result;
                }
            }
        }
    }
}
